from cartographer.manager import DrawingManager
from fields.field import Field
from fields.gradient import gradient, Gradient

if __name__ == "__main__":
    f1 = Field.from_csv("data/7x7.field")
    f1.values = f1.values / f1.values.max()

    dm = DrawingManager(f1.shape)
    dm.add_layer(f1, "ColorPolygon", {"color scale": "grey"})

    g1: Gradient = gradient(f1)
    dm.add_layer(g1, "Gradient", {"color scale": "color"})

    dm.draw()
    dm.save()
