import pandas as pd


class Field:
    def __init__(self, shape: tuple[int, int]):
        self.shape = shape
        self.values = None

    @property
    def rows(self):
        return self.shape[0]

    @property
    def columns(self):
        return self.shape[1]

    def __getitem__(self, index: tuple[int, int]):
        if 0 > index[0] or index[0] >= self.rows:
            return None
        elif 0 > index[1] or index[1] >= self.columns:
            return None
        return self.values[index]

    def __setitem__(self, index: tuple[int, int], value):
        if 0 > index[0] or index[0] >= self.rows:
            raise ValueError
        elif 0 > index[1] or index[1] >= self.columns:
            raise ValueError
        self.values[index] = value

    @classmethod
    def from_csv(cls, filename):
        x = pd.read_csv(filename, header=None)
        result = Field(x.shape)
        result.values = x.to_numpy()
        return result
