from dataclasses import dataclass
import numpy as np


def neighbours(position):
    if position[0] % 2:
        v = (-1, 1), (0, 1), (1, 1), (1, 0), (0, -1), (-1, 0)
    else:
        v = (-1, 0), (0, 1), (1, 0), (1, -1), (0, -1), (-1, -1)
    return [(position[0] + x, position[1] + y) for x, y in v]


@dataclass
class Flow:
    A: float
    B: float
    C: float
    D: float
    E: float
    F: float

    def __str__(self):
        return f"{[self.A, self.B, self.C, self.D, self.E, self.F]}"


class Gradient:
    def __init__(self, shape: tuple[int, int], values):
        self.shape = shape
        self.full_shape = (shape[0] * 2 - 1, shape[1] * 2 - 1)
        self.values = np.array(values)

    @property
    def rows(self):
        return self.shape[0]

    @property
    def columns(self):
        return self.shape[1]

    def __getitem__(self, index: tuple[int, int]):
        if 0 > index[0] or index[0] >= self.rows:
            return None
        elif 0 > index[1] or index[1] >= self.columns:
            return None
        return self.values[i]


def interleave(a, b):
    result = []
    length = max(len(a), len(b))
    for i in range(length):
        if i < len(a):
            result.append(a[i])
        if i < len(b):
            result.append(b[i])
    return result



def gradient(field) -> Gradient:
    d0 = np.copy(field.values)
    d1 = np.diff(d0)
    d2 = np.diff(d0, axis=0)
    indexes = []
    for i in range(field.rows):
        if i % 2:
            indexes.append(field.rows*(i+1)-1)
        else:
            indexes.append(field.rows*i)
    _c3 = np.copy(d0)
    _c3 = np.delete(d0, indexes, None)
    _c3 = np.resize(_c3, (field.rows, field.columns-1))
    d3 = np.diff(_c3, axis=0)
    print(d0)
    print(d1)
    print(d2)
    print(d3)

    r1 = list()
    for x in range(7):
        r1.append(interleave(d0[x], d1[x]))
    r1 = np.array(r1)
    r1 = np.resize(r1, (field.rows, field.columns*2-1))
    print(r1)

    r2 = list()
    for x in range(6):
        r2.append(interleave(d2[x], d3[x]))
    r2 = np.array(r2)
    r2 = np.resize(r2, (field.rows, field.columns*2-1))
    print(r2)

    r3 = interleave(r1, r2)
    print(r3)

    return Gradient(field.shape, r3)