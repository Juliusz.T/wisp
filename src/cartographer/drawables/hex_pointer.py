from dataclasses import dataclass

from cartographer.drawables.constants import HEX
from cartographer.point import Point


@dataclass
class HexPointer:
    """Class that returns points assigned to hex

    Hex Coordinates in “odd-r” horizontal layout convention

    (0,0) (0,1) (0,2)
       (1,0) (1,1) (1,2)
    (2,0) (2,1) (2,2)
    """

    row: float
    column: float

    @property
    def center(self) -> Point:
        row_offset = (-1 * abs((self.row % 2)-1) + 1) * (HEX.HORIZONTAL_SPACING / 2)
        offset = Point(x=row_offset + HEX.WIDTH / 2 - 1, y=HEX.HEIGHT / 2)

        return offset + Point(x=int(self.column * HEX.WIDTH), y=int(self.row * HEX.VERTICAL_SPACING))

    def polygon(self, scale=1) -> list[Point]:
        A = Point(0, -HEX.SIDE * scale)
        B = Point(HEX.WIDTH / 2 * scale, -HEX.SIDE / 2 * scale)
        C = Point(HEX.WIDTH / 2 * scale, HEX.SIDE / 2 * scale)
        D = Point(0, HEX.SIDE * scale)
        E = Point(-HEX.WIDTH / 2 * scale, HEX.SIDE / 2 * scale)
        F = Point(-HEX.WIDTH / 2 * scale, -HEX.SIDE / 2 * scale)

        return [self.center + i for i in [A, B, C, D, E, F]]

    def borders(self, scale=1) -> list[tuple[Point, Point]]:
        A, B, C, D, E, F = self.polygon(scale)
        return [(A, B), (B, C), (C, D), (D, E), (E, F), (F, A)]
