class HEX:
    SCALE: int = 1
    SIDE: int = 16 * SCALE
    WIDTH: int = round(3**(1/2) * SIDE)
    HEIGHT: int = 2 * SIDE
    HORIZONTAL_SPACING: int = WIDTH
    VERTICAL_SPACING: int = round(3/4 * HEIGHT)


class COLOR:
    BACKGROUND = (191, 191, 191, 255)
    WHITE = (255, 255, 255, 255)
    BLACK = (0, 0, 0, 255)
    RED = (255, 0, 0, 255)
    YELLOW = (255, 255, 0, 255)
    GREEN = (0, 255, 0, 255)
    CYAN = (0, 255, 255, 255)
    BLUE = (0, 0, 255, 255)
    MAGENTA = (255, 0, 255, 255)
