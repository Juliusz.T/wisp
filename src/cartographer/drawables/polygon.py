from dataclasses import dataclass

from PIL import ImageDraw

from cartographer.drawables.hex_pointer import HexPointer
from cartographer.point import Point


@dataclass
class Hex:
    row: int
    column: int
    value: float
    params: dict

    def __post_init__(self):
        if self.params is None:
            self.params = {}


@dataclass
class ScaledPolygon(Hex):
    @property
    def polygon(self):
        return HexPointer(self.row, self.column).polygon(self.value)

    @property
    def borders(self):
        return HexPointer(self.row, self.column).borders(self.value)

    @property
    def color(self) -> tuple[int, int, int, int]:
        if "color" in self.params.keys():
            return self.params["color"]
        return (128, 128, 128, 255)

    def draw(self, image, margin):
        draw = ImageDraw.Draw(image, "RGBA")
        draw.polygon(
            [(margin + p).tuple for p in self.polygon],
            fill=self.color,
        )
        for b in self.borders:
            draw.line(
                ((margin + b[0]).tuple, (margin + b[1]).tuple),
                fill=self.color,
            )


@dataclass
class ColorPolygon(Hex):
    @property
    def color(self):
        if "color scale" in self.params.keys():
            if self.params["color scale"] == "grey":
                return (int((1 - self.value) * 255), int((1 - self.value) * 255), int((1 - self.value) * 255), 255)
        return (int((1 - self.value) * 255), int((1 - self.value) * 255), int(self.value * 255), 255)

    @property
    def polygon(self):
        return HexPointer(self.row, self.column).polygon()

    @property
    def borders(self):
        return HexPointer(self.row, self.column).borders()

    def draw(self, image, margin):
        draw = ImageDraw.Draw(image, "RGBA")
        draw.polygon(
            [(margin + p).tuple for p in self.polygon],
            fill=self.color,
        )
        for b in self.borders:
            draw.line(
                ((margin + b[0]).tuple, (margin + b[1]).tuple),
                fill=self.color,
            )


@dataclass
class Gradient(Hex):
    radius = 4
    
    @property
    def center(self):
        return HexPointer(self.row, self.column).center

    @property
    def bounding_box(self):
        return [
            Point(self.center.x-self.radius/2, self.center.y-self.radius/2), 
            Point(self.center.x+self.radius/2, self.center.y+self.radius/2)
        ]


    @property
    def color(self):
        def red(v):
            if v > 0:
                return (int(v*255), 0, 0, 255)
            else:
                v = -v
                return (0, int(v*255), int(v*255), 255)

        def green(v):
            if v > 0:
                return (0, int(v*255), 0, 255)
            else:
                v = -v
                return (int(v*255), 0, int(v*255), 255)

        def blue(v):
            if v > 0:
                return (int(v*255), int(v*255), 0, 255)
            else:
                v = -v
                return (0, 0, int(v*255), 255)

        if (self.row % 1) == 0 and (self.column % 1) == 0:
            raise ValueError
        elif (self.row % 1) == 0 and (self.column % 1) != 0:
            return red(self.value)
        elif (self.row % 1) != 0:
            if (self.row - 0.5) % 2:
                if (self.column % 1) != 0:
                    return blue(self.value)
                elif (self.column % 1) == 0:
                    return green(self.value)
            elif (self.row + 0.5) % 2:
                if (self.column % 1) != 0:
                    return green(self.value)
                elif (self.column % 1) == 0:
                    return blue(self.value)


    def draw(self, image, margin):
        if (self.row % 1) == 0 and (self.column % 1) == 0:
            return
        draw = ImageDraw.Draw(image, "RGBA")
        draw.ellipse(
            [(margin + p).tuple for p in self.bounding_box],
            fill=self.color,
        )

