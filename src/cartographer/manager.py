from cartographer.drawables.polygon import ColorPolygon, ScaledPolygon, Gradient
from cartographer.drawer import Drawer


class Layer:
    def __init__(self, shape, values, type, params):
        self.shape = shape
        self.values = values
        self.type = type
        self.params = params

    @property
    def rows(self):
        return self.shape[0]

    @property
    def columns(self):
        return self.shape[1]

    @property
    def _type(self):
        if self.type == "ScaledPolygon":
            return ScaledPolygon
        if self.type == "ColorPolygon":
            return ColorPolygon
        if self.type == "Gradient":
            return Gradient

    def __iter__(self):
        hexes = []
        if self.type == "Gradient":
            for r in range(self.rows*2-1):
                for c in range(self.columns*2-1):
                    hexes.append(self._type(r/2, c/2, self.values[(r, c)], self.params))
        else:
            for r in range(self.rows):
                for c in range(self.columns):
                    hexes.append(self._type(r, c, self.values[(r, c)], self.params))

        return iter(hexes)


class DrawingManager:
    def __init__(self, shape):
        self.shape = shape
        self.layers = []
        self.images = []

    @property
    def rows(self):
        return self.shape[0]

    @property
    def columns(self):
        return self.shape[1]

    def add_layer(self, field, type, params=None):
        self.layers.append(Layer(self.shape, field.values, type, params))

    def draw(self):
        d = Drawer(self.shape)
        for layer in self.layers:
            d.draw_layer(layer)
        image = d.render()
        self.images.append(image)
        return image

    def save(self, gif=False):
        if gif:
            path = "test.gif"
            self.images[0].save(path, save_all=True, append_images=self.images[1:], duration=len(self.images), loop=0)
        else:
            path = "test.png"
            self.images[-1].save(path)
