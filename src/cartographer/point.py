from dataclasses import dataclass
from typing import Tuple


@dataclass
class Point:
    """2D point for graphic engine"""

    x: float
    y: float

    @property
    def tuple(self) -> Tuple[(int, int)]:
        return (round(self.x), round(self.y))

    def __add__(self, point: "Point") -> "Point":
        return Point(
            x=self.x + point.x,
            y=self.y + point.y,
        )

    def __sub__(self, point: "Point") -> "Point":
        return Point(
            x=self.x - point.x,
            y=self.y - point.y,
        )

    def __mul__(self, scale: float | int) -> "Point":
        return Point(x=scale * self.x, y=scale * self.y)

    def __rmul__(self, scale: float | int) -> "Point":
        return self.__mul__(scale)

    def __abs__(self) -> "Point":
        return Point(x=abs(self.x), y=abs(self.y))

    def __neg__(self) -> "Point":
        return Point(x=-self.x, y=-self.y)

    def __eq__(self, other: "Point") -> bool:  # type: ignore
        if not isinstance(other, Point):
            return NotImplemented
        precision = 5
        threshold = pow(10, -precision)
        return abs(other.x - self.x) < threshold and abs(other.y - self.y) < threshold
