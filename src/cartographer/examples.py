import pandas as pd

from cartographer import DrawingManager


def read_data_and_draw():
    data = "/home/juliusz/work/wisp/data.csv"
    x = pd.read_csv(data, skiprows=1, header=None)
    d = DrawingManager(7, 7)
    for index, row in x.iterrows():
        j = 0
        for val in row:
            d.layers[0].hexes[index * 7 + j].scale = float(val)
            j += 1
    for t in range(100):
        d.draw()
        d.layers[0].update()
    d.save(gif=True)


if __name__ == "__main__":
    read_data_and_draw()
