from PIL import Image

from cartographer.drawables.constants import COLOR, HEX
from cartographer.point import Point


class Drawer:
    def __init__(self, shape):
        self.shape = shape
        self.margin_width = 2
        self.image = Image.new("RGBA", self.image_size.tuple, color=COLOR.BACKGROUND)

    @property
    def rows(self):
        return self.shape[0]

    @property
    def columns(self):
        return self.shape[1]

    @property
    def image_size(self):
        """Image size with margins"""
        return (
            Point(
                x=HEX.WIDTH * self.columns + (HEX.HORIZONTAL_SPACING / 2),
                y=HEX.VERTICAL_SPACING * self.rows + (HEX.HEIGHT / 4),
            )
            + 2 * self.margin
        )

    @property
    def margin(self):
        return Point(
            self.margin_width,
            self.margin_width,
        )

    def draw_layer(self, layer) -> Image:
        for element in layer:
            element.draw(self.image, self.margin)

    def render(self):
        resized = self.image.resize(self.image_size.tuple, Image.Resampling.LANCZOS)
        return resized
