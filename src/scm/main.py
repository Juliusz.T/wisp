#!/usr/bin/env python3

import typer

from scm.definitions import SRC_FOLDER, TESTS_FOLDER
from scm.utils import execute, state

app = typer.Typer()


@app.callback()
def main(
    dry_run: bool = typer.Option(False, "--dry-run", help="Print command without without executing it"),
):
    """Software configuration management utils"""
    if dry_run:
        state["dry_run"] = True


@app.command()
def test():
    """Run pytest"""
    execute(f"pytest {TESTS_FOLDER}")


@app.command()
def flake8():
    """Check code style, syntax and complexity"""
    execute(f"flake8 {SRC_FOLDER} {TESTS_FOLDER}")


@app.command()
def black():
    """Format code"""
    cmd = f"black {SRC_FOLDER} {TESTS_FOLDER}"
    execute(cmd)


@app.command()
def isort():
    """Sort imports"""
    cmd = f"isort {SRC_FOLDER} {TESTS_FOLDER}"
    execute(cmd)


@app.command()
def mypy():
    """Run static type checker"""
    cmd = f"mypy {SRC_FOLDER}"
    execute(cmd)


@app.command()
def style():
    """Run all static analysis tools"""
    black()
    isort()
    flake8()
    mypy()


if __name__ == "__main__":
    app()
