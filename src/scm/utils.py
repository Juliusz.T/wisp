import subprocess

state = {"dry_run": False}


def execute(cmd):
    global state
    print(cmd)
    if not state["dry_run"]:
        return_code = subprocess.call(cmd, shell=True)
        return return_code
    return None
